import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Fighter[] knights = makeTeam1();
        Fighter[] warriors = makeTeam2();
        Battle.moveRandomGround();
        Battle battle = new Battle(knights, warriors);
        battle.combat();
    }

    private static Knight makeRandomKnight() {
        Random rand = new Random();
        int baseHP = rand.nextInt(999 - 99) + 1;
        int wp = rand.nextInt(2);
        return new Knight(baseHP, wp);
    }

    private static Warrior makeRandomWarrior() {
        Random rand = new Random();
        int baseHP = rand.nextInt(999 - 99) + 1;
        int wp = rand.nextInt(2);
        return new Warrior(baseHP, wp);
    }

    private static Fighter[] makeTeam1() {
        Fighter[] knights = new Fighter[3];
        for (int i = 0; i < knights.length; i++) {
            knights[i] = makeRandomKnight();
        }
        return knights;
    }

    private static Fighter[] makeTeam2() {
        Fighter[] warriors = new Fighter[3];
        for (int i = 0; i < warriors.length; i++) {
            warriors[i] = makeRandomWarrior();
        }
        return warriors;
    }
}
