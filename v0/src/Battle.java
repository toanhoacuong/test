import java.util.Random;

/**
 * Created by thuanle on 3/9/16.
 */
public class Battle {
    private static final double RATE_WIN = 0.5;
    private static final int GROUND_BOUND = 999;

    public static int GROUND = 1;

    private Fighter[] mKnightTeam;
    private Fighter[] mWarriorTeam;

    public Battle(Fighter[] knights, Fighter[] warriors) {
        mKnightTeam = knights;
        mWarriorTeam = warriors;
    }

    /**
     * Move current ground to new random ground
     */
    public static void moveRandomGround() {
        Random rand = new Random();
        GROUND = rand.nextInt(GROUND_BOUND) + 1;
    }

    public void combat() {
        double pr = 0;
        for (int i = 0; i < mKnightTeam.length; i++) {
            double result = duel(mKnightTeam[i], mWarriorTeam[i]);

            pr += result;

            if (i == 0 && result >= RATE_WIN){
                moveRandomGround();
            }
        }
        pr /= mKnightTeam.length;

        System.out.println(" Battle result. pR = " + pr);
    }

    private double duel(Fighter fighter1, Fighter fighter2) {
        int score1 = fighter1.getRealHp();
        int score2 = fighter2.getRealHp();

        double pr = (score1 - score2 + 999) / 2000.0;
        return pr;
    }
}
