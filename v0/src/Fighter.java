/**
 * Created by thuanle on 3/9/16.
 */
public class Fighter {
    private int mBaseHp;
    private int mWp;

    public Fighter(int baseHp, int wp) {
        mBaseHp = baseHp;
        mWp = wp;
    }

    protected int getBaseHp() {
        return mBaseHp;
    }

    public int getRealHp(){
        return 0;
    }

    protected int getWp() {
        return mWp;
    }
}
