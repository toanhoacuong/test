/**
 * Created by thuanle on 3/9/16.
 */
public class Knight extends Fighter {
    public Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public int getRealHp() {
        if (Utility.isSquare(Battle.GROUND)) {
            return getBaseHp() * 2;
        }
        if (getWp() == 1) {
            return getBaseHp();
        } else if (getWp() == 0) {
            return (int) (getBaseHp() / 10.0);
        } else {
            return 0;
        }
    }
}
